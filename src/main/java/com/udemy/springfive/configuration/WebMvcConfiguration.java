package com.udemy.springfive.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.udemy.springfive.component.RequestTimeInterceptor;

/**
 * The Class WebMvcConfiguration.
 */
@Configuration
public class WebMvcConfiguration extends WebMvcConfigurerAdapter{

	/** The req time interceptor. */
	@Autowired
	@Qualifier("requestTimeInterceptor")
	private RequestTimeInterceptor reqTimeInterceptor;
	
	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#addInterceptors(org.springframework.web.servlet.config.annotation.InterceptorRegistry)
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		
		registry.addInterceptor(reqTimeInterceptor).excludePathPatterns("/css/**");

//		with xml configurationç
//		<mvc:interceptors>
//		<bean class="org.springframework.web.servlet.i18n.LocaleChangeInterceptor" />
//		<mvc:interceptor>
//		    <mvc:mapping path="/**"/>
//		    <mvc:exclude-mapping path="/admin/**"/>
//		    <bean class="org.springframework.web.servlet.theme.ThemeChangeInterceptor" />
//		</mvc:interceptor>
//		<mvc:interceptor>
//		    <mvc:mapping path="/secure/*"/>
//		    <bean class="org.example.SecurityInterceptor" />
//		</mvc:interceptor>
		
	}

}
