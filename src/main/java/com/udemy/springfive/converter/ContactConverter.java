package com.udemy.springfive.converter;

import org.springframework.stereotype.Component;

import com.udemy.springfive.entity.Contact;
import com.udemy.springfive.model.ContactModel;

/**
 * The Class ContactConverter.
 */
@Component("contactConverter")
public class ContactConverter {

	/**
	 * Convert to contact.
	 *
	 * @param contactModel the contact model
	 * @return the contact
	 */
	public Contact convertToContact(ContactModel contactModel) {
		return new Contact(contactModel.getId(), contactModel.getFirstName(),
				contactModel.getLastName(), contactModel.getCity(), contactModel.getPhone());
	}

	/**
	 * Convert 2 contact model.
	 *
	 * @param contact the contact
	 * @return the contact model
	 */
	public ContactModel convert2ContactModel(Contact contact) {
		return new ContactModel(contact.getId(), contact.getFirstName(), contact.getLastName(),
				contact.getCity(), contact.getPhone());
	}
}
