package com.udemy.springfive.service;

import java.util.List;

import com.udemy.springfive.entity.Contact;
import com.udemy.springfive.model.ContactModel;

/**
 * The Interface ContactService.
 */
public interface ContactService {

	/**
	 * Adds the contact.
	 *
	 * @param contactModel the contact model
	 * @return the contact model
	 */
	public abstract ContactModel addContact(ContactModel contactModel);

	/**
	 * List contacts.
	 *
	 * @return the list
	 */
	public List<ContactModel> listContacts();

	/**
	 * Find contact by id.
	 *
	 * @param id the id
	 * @return the contact
	 */
	public abstract Contact findContactById(int id);
	
	/**
	 * Find contact model by id.
	 *
	 * @param id the id
	 * @return the contact model
	 */
	public abstract ContactModel findContactModelById(int id);

	/**
	 * Removes the contact.
	 *
	 * @param id the id
	 */
	public abstract void removeContact(int id);
}
