package com.udemy.springfive.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.udemy.springfive.entity.UserRole;
import com.udemy.springfive.repository.UserRepository;

/**
 * The Class UserService.
 */
@Service("userService")
public class UserService implements UserDetailsService {

	/** The user rep. */
	@Autowired
	@Qualifier("userRepository")
	private UserRepository userRep;

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		com.udemy.springfive.entity.User user = userRep.findByUsername(username);
		List<GrantedAuthority> authorities = buildAuthorities(user.getUserRole());
		return buildUser(user, authorities);

	}

	/**
	 * SpringSec necestita crear un obteto User de Spring para gestionar la
	 * seguridad correctamente, con nuestros datos creamos el objeto.
	 *
	 * @param user the user
	 * @param authorities the authorities
	 * @return the user
	 */
	private User buildUser(com.udemy.springfive.entity.User user, List<GrantedAuthority> authorities) {

		User userBuild = new User(user.getUsername(), user.getPassword(), user.isEnable(), true, true, true,
				authorities);
		return userBuild;

	}

	/**
	 * SpringSec necesita que el objeto User tenga una lista de GrantedAuthority,
	 * asi que con nuestros roles y perfiles creamos la lista para el objeto User.
	 *
	 * @param userRoles the user roles
	 * @return the list
	 */
	private List<GrantedAuthority> buildAuthorities(Set<UserRole> userRoles) {

		Set<GrantedAuthority> auths = new HashSet<GrantedAuthority>();
		if (userRoles != null) {
			for (UserRole userRole : userRoles) {
				auths.add(new SimpleGrantedAuthority(userRole.getRole()));
			}
		}

		return new ArrayList<GrantedAuthority>(auths);

	}
}
