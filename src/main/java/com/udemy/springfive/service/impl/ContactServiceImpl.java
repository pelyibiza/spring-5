package com.udemy.springfive.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.udemy.springfive.converter.ContactConverter;
import com.udemy.springfive.entity.Contact;
import com.udemy.springfive.model.ContactModel;
import com.udemy.springfive.repository.ContactRepository;
import com.udemy.springfive.service.ContactService;

/**
 * The Class ContactServiceImpl.
 */
@Service("contactService")
public class ContactServiceImpl implements ContactService {

	/** The contact rep. */
	@Autowired
	@Qualifier(value = "contactRepository")
	ContactRepository contactRep;

	/** The contact converter. */
	@Autowired
	@Qualifier(value = "contactConverter")
	ContactConverter contactConverter;

	/* (non-Javadoc)
	 * @see com.udemy.springfive.service.ContactService#addContact(com.udemy.springfive.model.ContactModel)
	 */
	@Override
	public ContactModel addContact(ContactModel contactModel) {

		Contact contact = contactRep.save(contactConverter.convertToContact(contactModel));
		return contactConverter.convert2ContactModel(contact);

	}

	/* (non-Javadoc)
	 * @see com.udemy.springfive.service.ContactService#findContactById(int)
	 */
	@Override
	public Contact findContactById(int id) {

		return contactRep.findById(id);

	}

	/* (non-Javadoc)
	 * @see com.udemy.springfive.service.ContactService#findContactModelById(int)
	 */
	@Override
	public ContactModel findContactModelById(int id) {

		return contactConverter.convert2ContactModel(findContactById(id));

	}
	
	/* (non-Javadoc)
	 * @see com.udemy.springfive.service.ContactService#removeContact(int)
	 */
	@Override
	public void removeContact(int id) {

		Contact contact = contactRep.findById(id);
		if(contact != null) {
			contactRep.delete(contact);
		}

	}

	/* (non-Javadoc)
	 * @see com.udemy.springfive.service.ContactService#listContacts()
	 */
	@Override
	public List<ContactModel> listContacts() {

		List<Contact> listContacts = contactRep.findAll();
		ArrayList<ContactModel> listContactModels = new ArrayList<>();

		if (listContacts != null) {
			for (Contact contact : listContacts) {
				listContactModels.add(contactConverter.convert2ContactModel(contact));
			}
		}

		return listContactModels;

	}
}
