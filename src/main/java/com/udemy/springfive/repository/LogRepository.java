package com.udemy.springfive.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.udemy.springfive.entity.Log;

/**
 * The Interface LogRepository.
 */
@Repository(value="logRepository")
public interface LogRepository extends JpaRepository<Log, Serializable>{

}
