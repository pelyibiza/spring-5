package com.udemy.springfive.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.udemy.springfive.model.ContactModel;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/rest")
public class RestController {

	@GetMapping("/checkrest")
	public ResponseEntity<String> checkRest() {

		return new ResponseEntity<String>("OK", HttpStatus.OK);

	}
	
	@GetMapping("/contactmodel")
	public ResponseEntity<ContactModel> contactModel() {
		
		ContactModel contactModel = new ContactModel(1,"Carlos","Solcar Loscar","Madrid", "625362326");
				
		return new ResponseEntity<ContactModel>(contactModel, HttpStatus.OK);

	}
}
