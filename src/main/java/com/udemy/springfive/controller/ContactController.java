package com.udemy.springfive.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.udemy.springfive.constant.ViewConstant;
import com.udemy.springfive.model.ContactModel;
import com.udemy.springfive.service.ContactService;

/**
 * The Class ContactController.
 */
@Controller
@RequestMapping("/contacts")
public class ContactController {

	/** The log. */
	private static Log LOG = LogFactory.getLog(ContactController.class);

	/** The contact ser. */
	@Autowired
	@Qualifier(value = "contactService")
	ContactService contactSer;

	/**
	 * Cancel.
	 *
	 * @return the string
	 */
	@GetMapping("/cancel")
	private String cancel() {
		LOG.info("METHOD: cancel");
		return "redirect:/contacts/list?result=0";
	}

	/**
	 * List contacts.
	 *
	 * @param result the result
	 * @return the model and view
	 */
	@GetMapping("/list")
	@PreAuthorize("permitAll()")
	public ModelAndView listContacts(@RequestParam(name = "result", defaultValue = "-1") int result) {
		LOG.info("METHOD: listContacts");
		ModelAndView modelAndView = new ModelAndView(ViewConstant.VIEW_CONTACTS);

		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		modelAndView.addObject("username", user.getUsername());
		
		modelAndView.addObject("result", result);
		modelAndView.addObject("contacts", contactSer.listContacts());
		return modelAndView;
	}

	/**
	 * Redirect contact form.
	 *
	 * @param id the id
	 * @param model the model
	 * @return the string
	 */
	@GetMapping("/form")
	private String redirectContactForm(@RequestParam(name = "id", required = false, defaultValue = "0") int id,
			Model model) {
		LOG.info("METHOD: redirectContactForm -- PARAMS: id=" + id);

		ContactModel contactModel = new ContactModel();
		if (id != 0) {
			contactModel = contactSer.findContactModelById(id);
		}
		model.addAttribute("contactModel", contactModel);
		return ViewConstant.VIEW_CONTACT_FORM;
	}

	/**
	 * Adds the contact.
	 *
	 * @param contactModel the contact model
	 * @param model the model
	 * @return the string
	 */
	@PostMapping("/addcontact")
	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
	public String addContact(@ModelAttribute(name = "contactModel") ContactModel contactModel, Model model) {
		LOG.info("METHOD: addContact -- PARAMS: contactModel=" + contactModel.toString());

		if (null != contactSer.addContact(contactModel)) {
			return "redirect:/contacts/list?result=1";
		} else {
			return "redirect:/contacts/list?result=2";
		}
	}

	/**
	 * Removes the contact.
	 *
	 * @param id the id
	 * @return the model and view
	 */
	@GetMapping("/remove")
	private ModelAndView removeContact(@RequestParam(name = "id", required = true) int id) {
		LOG.info("METHOD: removeContact -- PARAMS: id=" + id);

		contactSer.removeContact(id);
		return listContacts(3);

	}
}
