package com.udemy.springfive.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.udemy.springfive.constant.ViewConstant;
import com.udemy.springfive.model.UserCredential;

/**
 * The Class LoginController.
 */
@Controller
public class LoginController {

	/** The log. */
	private static Log LOG = LogFactory.getLog(LoginController.class);
	
//	@GetMapping("/")
//	public String redirectToLogin() {
//
//		LOG.info("METHOD: redirectToLogin");
//		return "redirect:/login";
//	}

	/**
 * Show login form.
 *
 * @param model the model
 * @param error the error
 * @param logout the logout
 * @return the string
 */
@GetMapping({"/login","/"})
	public String showLoginForm(Model model, @RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout) {

		LOG.info("METHOD: showLoginForm -- PARAMS: error=" + error + " - loguot=" + logout);
		model.addAttribute("userCredentials", new UserCredential());
		model.addAttribute("error", error);
		model.addAttribute("logout", logout);
		return ViewConstant.VIEW_LOGIN;

	}

	/**
	 * Loginsuccess.
	 *
	 * @return the string
	 */
	@GetMapping("/loginsuccess")
	public String loginsuccess() {

		return "redirect:/contacts/list";

	}
	
//	@PostMapping("/loginCheck")
//	public String loginCheck(@ModelAttribute(name = "userCredentials") UserCredential userCredential) {
//
//		LOG.info("METHOD: loginCheck -- PARAMS: userCredential=" + userCredential.toString());
//		if (userCredential.getUsername().equals("user") && userCredential.getPassword().equals("pass")) {
//			return "redirect:/contacts/list";
//		} else {
//			return "redirect:/login?error";
//		}
//
//	}
}