package com.udemy.springfive.constant;

/**
 * The Class ViewConstant.
 */
public class ViewConstant {

	/** The view login. */
	public static String VIEW_LOGIN = "login";
	
	/** The view contacts. */
	public static String VIEW_CONTACTS = "contacts";
	
	/** The view contact form. */
	public static String VIEW_CONTACT_FORM = "contactform";

}
